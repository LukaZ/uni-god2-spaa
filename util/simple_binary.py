import pickle
from util.simple_json import SimpleJSONLib

class SimpleBinaryLib():
    
    @classmethod
    def load_rawdata(cls, path, asJSON=False):
        try:
            if asJSON:
                data = SimpleJSONLib.loadJSONFile(path)
                return data
            else:
                with open(path, 'rb') as dfile:
                    data = pickle.load(dfile) #koristimo pickle za deserijalizaciju podataka
                    return data
        except Exception as ex:
            print(repr(ex))
    
    @classmethod
    def save_rawdata(cls, path, data, asJSON=False):
        try:
            if asJSON:
                SimpleJSONLib.saveJSONFile(path, data)
            else:
                with open(path, 'wb') as data_file:
                    pickle.dump(data, data_file) #koristimo pickle da bismo serijalizovali u binarnu datoteku    
        except Exception as ex:
            print(repr(ex))