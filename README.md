# Prezentacija

## Uvod

-- siguran login za lozinku
-- cuvanje u binarnu datoteku

Za zavrsni ispit bio sam izabrao temu "Kviz" i ovo ce sluziti kao prezentacija toga sto sam uradio i kako je bio osmisljen projekat sa moje strane.

## Kratak opis 
Ideja projekta je bilo da se obezbedi aplikacija koja ce imati opciju da se uloguje, korisnik bi onda u zavisnosti od tipa naloga ( 'user' ili 'admin' ) mogao da proba da resava kvizove i u zavisnosti od toga koliko tacnih odgovora da, osvoji poene.

## Implementacija sistema ulogovanja
Jednostavna implementacija time sto korisnik prvo kada pokrene aplikaciju pita ga za korisnicko ime i lozinku, nakon toga ukoliko unese tacne podatke ispisuje mu se __modularni korisnicki interfejs__ koji cu u daljem tekstu objasniti funkcionalnost.

## Implementacija i struktura podataka i objasnjenje 
---
### __players.json__
#### Struktura fajla
    "<ID>": {
        "accountType": "user",
        "id": <ID>,
        "pwd": "<USER_PWD>",
        "score": 29,
        "uname": "luka"
    },

#### Opis
>Ovaj fajl sadrzi sve podatke o postojecim korisnicima, struktuiran je kao Key (< ID >),Value (Sami podatci o korisniku) tip podatka. Svaki unos u fajlu sadrzai Tip Naloga ( admin/user ), id ( Koji se poklapa sa K tu je samo radi jednostavnosti koriscenja ), Score ( Skor koji je korisnik postigao) i na kraju uname i pwd koje korisnik koristi za ulogovanje.
---
### __quizzes.json__

#### Struktura fajla
    "<IME_KVIZA>:{
        "<ID_PITANJA>":{
            "correct": "4",
            "options": {
                "1": "Beograd",
                "2": "Bec",
                "3": "Zurich",
                "4": "Bern"
            },
            "points": "25",
            "question": "Koji je glavni grad Svajcarske ?"
        }
    }
>U ovom fajlu se nalaze svi kvizovi i njihova odgovarajuca pitanja, Naime isto se skladiste podatci na princip kljuc, vrednost kao i kod igraca. < ID_PITANJA > sluzi kao jednistveni kljuc za jedno pitanje u datom kvizu. polje "correct" naznacava koje od opcija u "options" je tacan odgovor, znaci konkretno u datom primeru iznad tacan odgovor bi bio pod "4". Ovo sve je propraceno poljem 'question' koje se prikazuje igracu.
---
### Player klasa

Ova klasa je namenjena za koirsnika koji se uloguje u sistem. Unutar klase se nalaze neke metode koje dalje olaksavaju manipulacijom podataka tokom izvrsavanja programa.

----

## UI __staticka__ klasa

Prvo zelim da navedem da je ideja implementacije ove klase bila takva da funkcionise toj kao static klasa u C#, Imao sam prethodnog iskustva sa manipulacijom bazom podataka ( kasnije opisana kao _db.py_ ) gde sam kao glavnu metodu manipulacije bazom koristio staticku klasu, Ova klasa ( UI.py ) je osmisljena da na isti taj nacin funkcionise.

Naime, ovo je ujedno i najveci fajl u celom projektu i namenjena je za primanje inputa korisnika i da prosledjuje te inpute u Database klasu. Najistaknutija funkcionalnost ove klase se nalazi pri dnu gde je osmisljeni modularnu UI interfejs.

Funkcionise tako sto u zavisnosti od tipa korisnika koji se ulogovao (admin/user) prikazuju mu se odgovarajuce opcije. Naime ukoliko se uloguje admin nalog njemu se inicijativno ili na kraju neke operacije uvek prikazuje 'mainMenu' opcija.

Svaki unos u "mainMenu" ima kljuc i vrednost. Kluc ce biti prikazan kao "[1] "Tekst opcije" na korisnickom interfejsu pomocu petlje sve dok ne nestane opcija. Potom korisnik kada unese redni broj opcije koji zeli da koristi u petlji koja se nalazi u metodi "printModularMenu" prvo proverava da li postoji "onSelect" unos. Ukoliko postoji program skace na tu opciju u _ui listi.

Primer:

    admin": {  # administrator 
            "mainMenu": {
                1: {
                    "text": "Istampaj Hello World!",
                    "onSelect": "printHello",
                },
            },
            "printHello": {
                1: {
                    "text": "Da li ste sigurni da zelite da istampate "Hello poruku ?"
                    "onSelect" lambda: print("Hello world!")
                }
            }
    }

U datom primeru bilo bi nam ponudjena opcija pod rednim brojem 1 ( zato sto je kljuc u 'mainMenu' pod 1) sa tekstom "Istampaj Hello World!", potom nakon unosa broja 1 i pritiska enter program u petlji skace na meni "printHello" u _ui listi dict-u. Potom kada udje u tu opciju isto se stampaju sve opcije i kada opet selektujemo opciju 1 poziva se lambda funkcija koja na nas ekran stampa "Hello World!" ( Lambda funkcija je u sustini anonimna funkcija, morao sam da upotrebim lambdu jer ukoliko stavimo () na kraj neke funkcije u toj listi ona se odma pozove )

_Ovaj deo je malo grubo objasnjen jer sama funkcionalnost je dosta jednostavna problem je da tu funkcionalnost formiram u reci, ukoliko potrebno molim vas navedite da li zelite da vam usmeno objasnim ovaj deo nije nikakav problem._

---

## Database __staticka klasa__

Ova klasa je zaduzena za manipulacijom vec prosledjenih podataka sa strane UserInterface klase i manipulise nad njima i cuva ih u JSON fajlove nakon svake izmene.


Ukoliko ima nejasnih delova/primedbi/pitanja molim vas kontaktirajte me. Ja smatram da sam ovime dovoljno opisao da se stekne osnovna ideja funkcionalnosti aplikacije, upareno sa time da pokrenete samu aplikaciju mislim da se stice dovoljo detaljna ideja o tome kako aplikacija funkcionise i radi.

