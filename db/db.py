import json

from classes.player import Player
from classes.dll.dll import DoublyLinkedList
from classes.quizz import Quizz
from classes.question import Question

from util.simple_binary import SimpleBinaryLib

# Koriscen je svugde @classmethod kako bi emulirao funkcionalnost static modifikatora pristupa
# klase kao u C#

class Database():
    players = {}
    quizzes = None
    
    dataLoaded = False
    activePlayer = None

    @classmethod
    def addQuizz(cls, quizzName):
        if quizzName in list(cls.quizzes.keys()):
            print("\tKviz sa tacno tim imenom vec postoji!")
            return False
        cls.quizzes[quizzName] = {}
        return True

    @classmethod
    def deleteQuizz(cls, quizzName):
        if not quizzName in list(cls.quizzes.keys()):
            return False
        del cls.quizzes[quizzName]
        cls.saveQuizzes()
        return True

    @classmethod
    def deleteQuizQuestion(cls, quizName, quizQuestionID):
        del cls.quizzes[quizName][quizQuestionID]
        cls.saveQuizzes()
        return True

    @classmethod
    def addQuizzQuestion(cls, quizzName, questionData):
        newQuestionID = str( len( cls.quizzes[quizzName].keys() ) +1 )
        cls.quizzes[quizzName][newQuestionID] = questionData
        


    @classmethod
    def saveQuizzes(cls):
        SimpleBinaryLib.save_rawdata('./data/quizzes',cls.quizzes)



    @classmethod
    def loadQuizzes(cls): 
        try:
            cls.quizzes = DoublyLinkedList()
            
            _quizzDict = SimpleBinaryLib.load_rawdata('./data/quizzes')
            
            for quizTitle in _quizzDict:
                quizz = _quizzDict[quizTitle]
                _questionDict = []
                for quizzQuestionID in quizz:
                    quizzQuestion = quizz[quizzQuestionID]
                    _questionDict.append(
                        Question(
                            quizzQuestion['options'],
                            quizzQuestion['correct'],
                            quizzQuestion['points'],
                            quizzQuestion['question']
                        )
                    )
                cls.quizzes.append(Quizz(quizTitle, _questionDict))   
            
                
        except Exception as ex:
            print(repr(ex))


    @classmethod
    def updateActivePlayerScore(cls, score):
        for k in cls.players:
            ply = cls.players[str(k)]
            if ply['id'] == cls.activePlayer.id:
                ply['score'] = int(ply['score']) + int(score) # za svaki slucaj sve konvertuj u int
                cls.activePlayer.score += int(score)
                cls.savePlayers()

    @classmethod
    def savePlayers(cls):
        with open('./data/players.json', 'w') as outfile:
            json.dump(cls.players, outfile, indent=4, sort_keys=True )


    @classmethod
    def loadPlayers(cls): 
        try:
            # _plyDict = {}
            with open( './data/players.json', 'r') as fp:
                cls.players = json.load(fp)

        except Exception as ex:
            print(repr(ex))


    @classmethod
    def LoadData(cls):
        cls.loadPlayers()
        cls.loadQuizzes()
        
        cls.dataLoaded = True


    @classmethod
    def SaveData(cls):
        cls.savePlayers()
        cls.saveQuizzes()

    @classmethod
    def validateLogin(cls,uname,pwd):
        if not cls.dataLoaded:
            raise Exception("Podatci nisu ucitani!")
        for k in cls.players:
            v = cls.players[str(k)]
            if v['pwd'] == pwd and v['uname'] == uname:
                cls.activePlayer = Player(v['id'], v['uname'],  v['pwd'],  v['score'], v['accountType']) 
                return True
        
        return False
            
