import json
from db.db import Database as db
from classes.ui import UserInterface as ui

_players = {}
_questions = {}


# Iako je aplikacija struktuirana kao klijent/server tip njoj idalje nedostaje jasna linija izmedju klijentske/serverske strane, ovaje nedostatak je radi prakticnost i lakoce pisanja koda

db.LoadData()



exit = False
while not exit:  # continious ui display
    if ui.promptLogin():
        _exit = False
        while not _exit:
            _exit = ui.printModularMenu('mainMenu')
            if _exit == "Q" or "q":
                _exit = True
        exit = True
    else:
        print("Netacan login ili nalog nepostoji")
