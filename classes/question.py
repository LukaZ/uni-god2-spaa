
class Question():
    def __init__(self, options, correct, points, question):
        self.options = options
        self.correct = correct
        self.points = points
        self.question = question
    
    
    def getQuestion(self):
        return self.question

    
    def getOptions(self):
        return self.options

    
    def isCorrectAnswer(self, optionID):
        return self.correct == optionID


    def getPoints(self):
        return int(self.points)



