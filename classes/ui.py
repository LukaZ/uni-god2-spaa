import sys, random, getpass
from db.db import Database as db


class UserInterface:
    

    @classmethod
    def welcomeMessage(cls):
        print(f"""
            ===========================================
            \tDobrodosli nazad {db.activePlayer.uname}
            \tVas trenutni skor je {db.activePlayer.score or 0}
            ===========================================""")
        

    @classmethod
    def promptLogin(cls):
        uname = input("Enter username: ")
        pwd = getpass.getpass("Enter password: ")
        bsucc = db.validateLogin(uname,pwd) 
        if bsucc:
            UserInterface.welcomeMessage()
        return bsucc
        
    @classmethod
    def createNewQuiz(cls):
        quizName = input("Unesite naziv kviza:")
        if not db.addQuizz(quizName):
            cls.createNewQuiz()
        else:
            print("Da li zelite odma da sacuvate promene?")
            cls.printModularMenu('saveChangesQuizzes')
        

    @classmethod
    def printQuizzes(cls):
        print("Na raspolaganju imamo sledece kvizove:")
        printout = {}
        i = 1
        for k in db.quizzes:
            print(f"[{i}] {k}")
            printout[str(i)]=k
            i+=1
        return printout


    @classmethod
    def removeQuizz(cls):
        printout = cls.printQuizzes()
        select = input("Koji kviz zelite da izbrisete ?")
        if input(f'Da li ste sigurni da zelite da izbrisete kviz \"{printout[select]}\" i sva njegova pitanja (Y/n) ?: ') == "Y":
            if db.deleteQuizz( printout[select] ):
                print(f"Uspesno obrisan kviz {printout[select]}")

    @classmethod
    def printAllQuizQuestions(cls, selectedQuiz):
        questionPrintout = {}
        questionPrintoutCounter = 1
        for quizzID in selectedQuiz:
            currentQuestion = selectedQuiz[quizzID]
            questionPrintout[questionPrintoutCounter] = currentQuestion
            print(f'Pitanje: [{questionPrintoutCounter}]:\"{currentQuestion["question"]}\"')
            questionPrintoutCounter += 1
        return questionPrintout


    @classmethod
    def removeQuizzQuestion(cls):
        printout = cls.printQuizzes()
        select = input("U kom kvizu se nalazi pitanje koje zelite da obrisete ?")
        selectedQuiz = printout[select]
        printout = cls.printAllQuizQuestions(db.quizzes[selectedQuiz])
        deleteQuestionID = input("Koje pitanje iz kviza zelite da izbrisete?:")
        if db.deleteQuizQuestion(selectedQuiz, deleteQuestionID):
            print("Uspesno obrisano pitanje!")
        
        

    @classmethod
    def addQuizzQuestions(cls):
        printout = cls.printQuizzes()
        select = input("Na koji kviz zelite da dodate pitanja?: ")

        newQuizQuestion = {}
        newQuizQuestion['question'] = input("Unesite zeljeni tekst pitanja: ")
        newQuizQuestion['points'] = input("Unesite koliko poena treba nagraditi za korektan odgovor: ")
        newQuizQuestion['options'] = {}
        
        print("Pocnite da unosite moguce odgovore na pitanje, \n kada zavrsite unesite 'q' u konzolu")
        exitInput = False
        enteredAnswers = 1
        while not exitInput:
            answerOptionInput = input(f"[{enteredAnswers}]: ")
            if str.lower(answerOptionInput) == 'q':
                if not enteredAnswers > 2:
                    print("Morate uneti barem 2 moguca odgovora!")
                else:
                    exitInput = True
            else:
                newQuizQuestion['options'][str(enteredAnswers)] = answerOptionInput
                print(f"Uneto: [{enteredAnswers}] {answerOptionInput} !")
                enteredAnswers += 1
        cls.printQuizQuestion(newQuizQuestion)
        newQuizQuestion['correct'] = input("Unesite koji od unetih odgovora bi se racunao kao tacan odgovor: ")

        print(newQuizQuestion)
        db.addQuizzQuestion(printout[select],newQuizQuestion)
        
        print("Da li zelite odma da sacuvate promene?")
        cls.printModularMenu('saveChangesQuizzes')


    @classmethod
    def pickQuiz(cls):
        cls.printQuizzes()
        select = input("Koji kviz zelite da probate da uradite? : ")
        cls.printModularQuizzes(int(select))


    @classmethod
    def printScore(cls):
        print(f"Vas skor je {db.activePlayer.score}")


    @classmethod
    def printQuizQuestion(cls, question):
        print(question.getQuestion())
        questionOptions = question.getOptions()
        for optionID in questionOptions:
            optionText = questionOptions[optionID]#question['options'][optionID]
            print(f"\t [{optionID}] {optionText}")
        
    @classmethod
    def promptQuestion(cls, question):
        cls.printQuizQuestion(question)

        answer = input("Unesite redni broj onoga sto vi mislite da je tacan odgovor: ")
        try:
            if question.isCorrectAnswer(int(answer)):
                wonPoints = question.getPoints()
                print(f"Tacno ste odgovorili na pitanje i osvojili {wonPoints} poena!")
                db.updateActivePlayerScore(wonPoints)
                return True
            elif int(answer) > len(question.getOptions()):
                print(f"Ne postoji ponudjen odgovor pod brojem {answer}")
            else:
                print("Netacno!")
        except ( KeyError, ValueError ):  # uneta pogresna ili nepostojeca vrednost
            print(f"Ne postoji ponudjen odgovor pod brojem {answer}")
        
        return False

    @classmethod
    def printModularQuizzes(cls, quizSection):
        selectedQuiz = db.quizzes[quizSection]
        _alreadyAsnwered = {}

        for question in selectedQuiz.questions:
            if question not in _alreadyAsnwered:
                cls.promptQuestion(question)
                _alreadyAsnwered[question] = True


    @classmethod
    def printModularMenu(cls, menuSection):
        """
            Ispisuje glavni meni sve dok se ne unsese "q" ili "Q"
        """

        result = False
        acclvl = db.activePlayer.getAccountType()

        for k, v in _uiMenus[acclvl][menuSection].items():
            if k == "function":
                _uiMenus[acclvl][menuSection]["function"]()
            if "text" in v:
                print("["+str(k)+"] "+v["text"])
        print("[Q] Izadji iz aplikacije...")

        result = input("Unesite broj zeljene akcije:: ")

        if str.lower(result) == "q":
            db.SaveData()
            return True

        try:
            _menu = _uiMenus[acclvl][menuSection][int(result)] # pribavljanje kojeg menija smo u 'mainMenu', etc...
            
            _onSelect = _menu["onSelect"] if "onSelect" in _menu else None
            _function = _menu["function"] if "function" in _menu else None

            onSelectPrint = _onSelect != None and _onSelect != ""
            onSelectFunc = _function != None and _function != ""

            if onSelectPrint:
                # prikazi sledeci meni ukoliko ima onSelect polje
                cls.printModularMenu(_onSelect)
            if onSelectFunc:
                # ako dodjemo do ovde, znaci da nema onselect vec ima samo funkcija
                _menu["function"]()

            # gotovi sa funkcijom, to je jedini nacin da cemo doci do ove linije, znaci vrati na pocetni meni
            cls.printModularMenu("mainMenu")
        except ( KeyError, ValueError ):  # uneta pogresna ili nepostojeca vrednost
            print("Ne postoji opcija "+str(result)+" u meniju "+str(k))
            cls.printModularMenu(menuSection)  # prikazi mu isti meni opet
        except:
            print("Unexpected error:", sys.exc_info()[0])
        return result


_uiMenus = {
    "admin": {  # administrator 
        "mainMenu": {
            1: {
                "text": "Izaberi kviz",
                "function": UserInterface.pickQuiz,
            },
            2: {
                "text": "Prikazi moj skor",
                "function": UserInterface.printScore,
            },
            3: {
                "text": "Dodaj novi kviz",
                "function": UserInterface.createNewQuiz,
            },
            4: {
                "text": "Obrisi kviz i sva pitanja",
                "function": UserInterface.removeQuizz
            },
            5: {
                "text": "Obrisi pitanje iz kviza",
                "function": UserInterface.removeQuizzQuestion
            },
            6: {
                "text": "Dodaj pitanje u vec postojeci kviz",
                "function": UserInterface.addQuizzQuestions,
            },
        },
        "saveChangesQuizzes":{
            1: {
                "text":"Da",
                "function": db.saveQuizzes
            },
            2: {
                "text":"Ne"
            }
        },
    },
    "user": {  # Korisnik
        "mainMenu": {
            1: {
                "text": "Izaberi kviz",
                "function": UserInterface.pickQuiz,
            },
            2: {
                "text": "Prikazi moj skor",
                "function": UserInterface.printScore,
            },
            
        },
    },
}


