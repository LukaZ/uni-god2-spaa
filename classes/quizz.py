
class Quizz(object):
    def __init__(self, name, questions):
        self.name = name
        self.questions = questions

    def getName(self):
        return self.name

    def __str__(self):
        return str(self.name)