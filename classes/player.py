
class Player(object):
    
    def __init__(self, id, uname, pwd, score=0, accountType='user'):
        self.id = id
        self.uname = uname
        self.pwd = pwd
        self.score = int(score)
        self.accountType = accountType

    def __str__(self):
        return str(self.__dict__)
    
    def tryLogin(self, uname, pwd):
        return self.uname == uname and self.pwd == pwd
    
    def getAccountType(self):
        return self.accountType
